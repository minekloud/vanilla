#!/bin/sh
set -e

SERVER_PROPERTIES_FILE=/data/server.properties

log(){
  echo "[init] $*"
}

setServerPropertie(){
  local propertie=$1
  local value=$2
  if [ -n "$value" ]; then
    log "Setting ${propertie} to '${value}' in ${SERVER_PROPERTIES_FILE}"
    sed -i "/^${propertie}\s*=/ c ${propertie}=${value}" "$SERVER_PROPERTIES_FILE"
  #else
    #log "Skip setting ${propertie}"
  fi
}

echo "eula=${EULA:=false}" > /data/eula.txt

if [ ! -f "$SERVER_PROPERTIES_FILE" ]
then
   log "$SERVER_PROPERTIES_FILE does not exist. Copying from template"
   cp /srv/server.properties.template $SERVER_PROPERTIES_FILE
fi

setServerPropertie "server-name" "$PROPERTIE_PROPERTIE_SERVER_NAME"
setServerPropertie "server-ip" "$PROPERTIE_SERVER_IP"
setServerPropertie "server-port" "$PROPERTIE_SERVER_PORT"
setServerPropertie "motd" "$PROPERTIE_MOTD"
setServerPropertie "allow-nether" "$PROPERTIE_ALLOW_NETHER"
setServerPropertie "announce-player-achievements" "$PROPERTIE_ANNOUNCE_PLAYER_ACHIEVEMENTS"
setServerPropertie "enable-command-block" "$PROPERTIE_ENABLE_COMMAND_BLOCK"
setServerPropertie "spawn-animals" "$PROPERTIE_SPAWN_ANIMALS"
setServerPropertie "spawn-monsters" "$PROPERTIE_SPAWN_MONSTERS"
setServerPropertie "spawn-npcs" "$PROPERTIE_SPAWN_NPCS"
setServerPropertie "spawn-protection" "$PROPERTIE_SPAWN_PROTECTION"
setServerPropertie "generate-structures" "$PROPERTIE_GENERATE_STRUCTURES"
setServerPropertie "view-distance" "$PROPERTIE_VIEW_DISTANCE"
setServerPropertie "hardcore" "$PROPERTIE_HARDCORE"
setServerPropertie "snooper-enabled" "$PROPERTIE_SNOOPER_ENABLED"
setServerPropertie "max-build-height" "$PROPERTIE_MAX_BUILD_HEIGHT"
setServerPropertie "force-gamemode" "$PROPERTIE_FORCE_GAMEMODE"
setServerPropertie "max-tick-time" "$PROPERTIE_MAX_TICK_TIME"
setServerPropertie "enable-query" "$PROPERTIE_ENABLE_QUERY"
setServerPropertie "query.port" "$PROPERTIE_QUERY_PORT"
setServerPropertie "enable-rcon" "$PROPERTIE_ENABLE_RCON"
setServerPropertie "rcon.password" "$PROPERTIE_RCON_PASSWORD"
setServerPropertie "rcon.port" "$PROPERTIE_RCON_PORT"
setServerPropertie "max-players" "$PROPERTIE_MAX_PLAYERS"
setServerPropertie "max-world-size" "$PROPERTIE_MAX_WORLD_SIZE"
setServerPropertie "level-name" "$PROPERTIE_LEVEL"
setServerPropertie "level-seed" "$PROPERTIE_SEED"
setServerPropertie "pvp" "$PROPERTIE_PVP"
setServerPropertie "generator-settings" "$PROPERTIE_GENERATOR_SETTINGS"
setServerPropertie "online-mode" "$PROPERTIE_ONLINE_MODE"
setServerPropertie "allow-flight" "$PROPERTIE_ALLOW_FLIGHT"
setServerPropertie "level-type" "$PROPERTIE_LEVEL_TYPE"
setServerPropertie "resource-pack" "$PROPERTIE_RESOURCE_PACK"
setServerPropertie "resource-pack-sha1" "$PROPERTIE_RESOURCE_PACK_SHA1"
setServerPropertie "player-idle-timeout" "$PROPERTIE_PLAYER_IDLE_TIMEOUT"
setServerPropertie "broadcast-console-to-ops" "$PROPERTIE_BROADCAST_CONSOLE_TO_OPS"
setServerPropertie "broadcast-rcon-to-ops" "$PROPERTIE_BROADCAST_RCON_TO_OPS"
setServerPropertie "enable-jmx-monitoring" "$PROPERTIE_ENABLE_JMX"
setServerPropertie "sync-chunk-writes" "$PROPERTIE_SYNC_CHUNK_WRITES"
setServerPropertie "enable-status" "$PROPERTIE_ENABLE_STATUS"
setServerPropertie "entity-broadcast-range-percentage" "$PROPERTIE_ENTITY_BROADCAST_RANGE_PERCENTAGE"
setServerPropertie "function-permission-level" "$PROPERTIE_FUNCTION_PERMISSION_LEVEL"
setServerPropertie "network-compression-threshold" "$PROPERTIE_NETWORK_COMPRESSION_THRESHOLD"
setServerPropertie "op-permission-level" "$PROPERTIE_OP_PERMISSION_LEVEL"
setServerPropertie "prevent-proxy-connections" "$PROPERTIE_PREVENT_PROXY_CONNECTIONS"
setServerPropertie "use-native-transport" "$PROPERTIE_USE_NATIVE_TRANSPORT"
setServerPropertie "enforce-whitelist" "$PROPERTIE_ENFORCE_WHITELIST"

setServerPropertie "difficulty" "$PROPERTIE_DIFFICULTY"
setServerPropertie "gamemode" "$PROPERTIE_GAMEMODE"


# JVM Options

JVM_MaxRAMPercentage=${JVM_MaxRAMPercentage:=80.0}
log "Starting server with MaxRAMPercentage: $JVM_MaxRAMPercentage"

JVM_OPTS="-XX:MaxRAMPercentage=${JVM_MaxRAMPercentage} ${JVM_OPTS}"

JVM_OPTS="${JVM_OPTS}
  -XX:+UseG1GC
  -XX:+ParallelRefProcEnabled
  -XX:MaxGCPauseMillis=200
  -XX:+UnlockExperimentalVMOptions
  -XX:+DisableExplicitGC
  -XX:G1NewSizePercent=30
  -XX:G1MaxNewSizePercent=40
  -XX:G1HeapRegionSize=8M
  -XX:G1ReservePercent=20
  -XX:G1HeapWastePercent=5
  -XX:G1MixedGCCountTarget=4
  -XX:InitiatingHeapOccupancyPercent=15
  -XX:G1MixedGCLiveThresholdPercent=90
  -XX:G1RSetUpdatingPauseTimePercent=5
  -XX:SurvivorRatio=32
  -XX:+PerfDisableSharedMem
  -XX:MaxTenuringThreshold=1
  -Dusing.aikars.flags=https://mcflags.emc.gs
  -Daikars.new.flags=true
  "


log "Starting server"
exec /srv/mc-server-runner -debug java $JVM_OPTS -jar /srv/server.jar